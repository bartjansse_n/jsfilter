# jsFilter 

jsFilter offers a simple way to filter content with simple buttons


## Motivation

jsFilter is created to filter simple contentblocks(articles) for a portfolio projects overview layout.  


## Installation / Setup

A step by step instruction for jsFilter

6. 	Before we start you need to include jQuery into your project, it requires jQuery version 1.9.1 or higher.
		( https://jquery.com/download/ )

5. 	Add the filter.js to you directory, and load it in the bottom of your HTML code 
		(<script src="YOUR/PATH/filter.js"></script>)

4. Filter setup in HTML

---
Button setup

Notice: it is not mandatory to use <button> or <article>, but the jsf attributes are required.

How to:

You can simply add buttons to your filter with the <button> tag. Buttons require a 'jsf-button' classname and a 'jsf-cat' to set a category. The category which is set will be ussed to filter the articles.  

	   	Example:
	   		<button class="jsf-button" jsf-cat="yellow">yellow</button>
	   
   	   
How to show All articles

How to:

If you want to show all of your articles you need to make your 'jsf-cat' of the button 'all'.
The 'all' will also work in capitals.

	   	Example:	
	   		<button class="jsf-button" jsf-cat="ALL">all</button>

---
Article setup

How to:

You can add articles to your HTML with the <article> tag. An Article requires a 'jsf-article' classname and a 'jsf-cat'. The 'jsf-cat' is a category that you already made while adding the buttons.

You can add a transition animation to your filter, fill in a value in the 'jsf-animate'
'jsf-animate' is Optional. Specifies the speed of the show effect. Default value is 400 milliseconds
Possible values:
	milliseconds
	"slow"
	"fast"

	   	Example:
	   		<article class='jsf-article' jsf-cat='yellow' jsf-animate='300'></article>


4. So the classes 'jsf-button', 'jsf-article' and the category 'jsf-cat' are required to add in your HTML!

5. Enjoy the filter!



## Example

In the Demo directory you find a exemple of the jsFilter. Open the index.html in your browser.
There are 3 buttons and articels with different category's, and there is a button that shows all the articels called 'all'.
The arcticles can be simply filtered by category.


## Test Tools
My favorite web browser to develop is Google Chrome.
I used the Elements viewer and Console panel in Google Chrome to develop and test my Framework. 

###Console panel:
Log diagnostic information during development or interact with the JavaScript on the page.
The Console is easy to use to readout arrays or to check if a variable has been set in a statement.

###Elements viewer:
With Elements viewer you can edit styles, DOM, animations and edit it live.

For more information and tools checkout:
https://developers.google.com/web/tools/chrome-devtools


## Author

Bart Janssen
https://www.linkedin.com/in/bart-janssen-5b411b117/


## License
Free licene, Open Source Code by Bart Janssen.