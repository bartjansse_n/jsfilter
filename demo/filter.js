// jsFilter version 1.1
// by Bart Janssen
// Free licence
jsFilter = function(){


	// array with button category's
	this.category = new Array();

	$('.jsf-button').each(function(get){

		// push all button category's in array 'category'
        category.push($(this).attr('jsf-cat'));

    });


   // array with article category's
   this.article = new Array();

	$('.jsf-article').each(function(get){

		// push all article properties in array 'article'
        article.push( {
		     cat: 		$( this ).attr( 'jsf-cat' ),
		     animate: 	$( this ).attr( 'jsf-animate' ),
		   });

    });

	//anmiation default setting
	var defaultAnimation = '400';
	
	
    // button onclick action
	$('.jsf-button').click(function(){
	
		// get active category from button
		var activeCat = $(this).attr('jsf-cat');
		
		for (i = 0; i < article.length; i++) {

		  // looks if category is 'ALL', if it is all then show all article's
		    if (activeCat === 'ALL' || activeCat === 'all'){
			   	$(".jsf-article").show(defaultAnimation);

		    }else{
			  	if (article[i]['cat'] == activeCat) {
			  			// make's article's with active category visible(show)
				  		var activeFilter = '.jsf-article[jsf-cat='+activeCat+']';

				  		//check if animation is set
				  		if (article[i]['animate'] != '') {
				  			$(activeFilter).show(article[i]['animate']);
				  		}else{
				  			$(activeFilter).show(defaultAnimation);
				  		}
				}
				else{
				  		// hides article's with different category's than active category
				  		var hideFilter = '.jsf-article[jsf-cat='+article[i]['cat']+']';
						
						//check if animation is set
						if (article[i]['animate'] != '') {
							$(hideFilter).hide(article[i]['animate']);
						}else{
							$(hideFilter).hide(defaultAnimation);
						}
				}
		    }
		}

	});	

}


jsFilter();